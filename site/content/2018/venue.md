---
title: Local
---

# Local

Temos o prazer de anunciar que a MiniDebConf Curitiba 2018 acontecerá no Campus
Central da
[UTFPR - Universidade Tecnológica Federal do Paraná](http://www.utfpr.edu.br/curitiba/o-campus/pasta2),
um ponto de referência localizado próximo ao centro de Curitiba.

![Campus central da UTFPR](/2018/images/utfpr-externa.jpg "Campus central da UTFPR")

Foto de Fernando Luiz Scherer

## Localização

O Campus central da UTFPR está localizada na Av. Sete de Setembro, 3165.

  [hyperlink]: https://www.openstreetmap.org/way/427929017#map=17/-25.43967/-49.26734
  [image]: /2018/images/utfpr-mapa.png "Mapa da UTFPR"

[![Mapa da UTFPR][image]][hyperlink]

## Como chegar

O Campus central da UTFPR - Universidade Tecnológica Federal do Paraná fica em
frente ao [Shopping Estação](http://www.shoppingestacao.com.br), por isso esse
é o principal ponto de referência para quem vai de ônibus.

Para quem vai do aeroporto, existem dois ônibus:

* Aeroporto Executivo: a passagem custa R$ 15,00. O ponto mais próximo para
  descer é no Shopping Estação, que fica em frente a UTFPR, e ir andando.
  [Mais informações](https://www.aeroportoexecutivo.com.br).
* Linha Direta E32 Aeroporto/Terminal Boqueirão: também chamado de Ligeirinho
  Aeroporto
  ([ônibus cinza](http://www.gazetadopovo.com.br/ra/mega/Pub/GP/p4/2017/04/10/Curitiba/Imagens/Futuro/WhatsApp%20Image%202017-04-10%20at%2009.00.37.jpeg)),
  oferece transporte do Aeroporto Afonso Pena até o Terminal do Boqueirão. É a
  opção mais barata (R$ 4,25) mas você deverá descer no Terminal do Boqueirão e
  pegar outro ônibus, o "Ligeirão Boqueirão"
  ([ônibus bi-articulado azul](https://wiki.debconf.org/upload/0/01/Curitiba-biarticulado.jpg)).
  Você não precisará pagar mais uma passagem porque o Terminal do Boqueirão é
  de integração. No Ligeirão Boqueirão você deverá descer na estação tubo UTFPR,
  que fica ao lado do campus da UTFPR. Apesar de precisar pegar 2 ônibus, essa
  opção é bastante segura e fácil.
* De taxi do aeroporto para a UTFPR o valor deve ser de R$ 70,00. Lembre-se de
  avisar que você quer ir para o Campus central, que fica ao lado do Shooping
  Estação.
* Você pode usar os aplicativos Uber, Cabify e 99POP. O valor deve ficar em
  torno de R$ 40,00 para ir do Aeroporto até a UTFPR.

[Itinerário do Ligeirinho aeroporto](https://github.com/romuloceccon/itinerarios-onibus-curitiba/blob/master/itinerarios/AEROPORTO%20%28358%29.yml)
- descer na Estação Tubo Rodoferroviária

## Miniauditório

Fotos da MiniDebConf Curitiba 2017

![UTFR Miniauditório 1](/2018/images/auditorio-1.jpg "UTFR Miniauditório")

![UTFR Miniauditório 2](/2018/images/auditorio-2.jpg "UTFR Miniauditório")

![UTFR Miniauditório 3](/2018/images/auditorio-3.jpg "UTFR Miniauditório")

![UTFR Miniauditório 4](/2018/images/auditorio-4.jpg "UTFR Miniauditório")

## Hackerspace

Fotos da MiniDebConf Curitiba 2017

![UTFR Hackerspace 1](/2018/images/hackerspace-1.jpg "UTFR Hackerspace")

![UTFR Hackerspace 2](/2018/images/hackerspace-2.jpg "UTFR Hackerspace")

![UTFR Hackerspace 3](/2018/images/hackerspace-3.jpg "UTFR Hackerspace")

## Secretaria e loja

Fotos da MiniDebConf Curitiba 2017

![UTFR Secretaria 1](/2018/images/secretaria-1.jpg "UTFR Secretaria")

![UTFR Secretaria 2](/2018/images/secretaria-2.jpg "UTFR Secretaria")

![UTFR Secretaria e loja](/2018/images/secretaria-3.jpg "UTFR Secretaria e loja")


## Jardim aberto

![UTFR Jardim 1](/2018/images/jardim-1.jpg "UTFR Jardim")

![UTFR Jardim 2](/2018/images/jardim-2.jpg "UTFR Jardim")

![UTFR Jardim 3](/2018/images/jardim-3.jpg "UTFR Jardim")

![UTFR Jardim 4](/2018/images/jardim-4.jpg "UTFR Jardim")

## Lanchonete interna

![UTFR Lanchonete](https://wiki.debconf.org/upload/thumb/7/70/Curitiba-utfpr-15.jpg/500px-Curitiba-utfpr-15.jpg "UTFR Lanchonete")

![UTFR Lanchonete](https://wiki.debconf.org/upload/thumb/2/25/Curitiba-utfpr-25.jpg/500px-Curitiba-utfpr-25.jpg "UTFR Lanchonete")
