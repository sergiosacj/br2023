---
title: Certificados
---

# Certificados de participação

## Certificados emitidos

A lista de certificados que foram assinados durante o evento pode ser obtida
aqui, em duas versões idênticas, assinadas com GnuPG:

* uma [assinada por Daniel Lenharo](certificates.lenharo.asc), que organizou o
  processo de emissão dos certificados.
* uma [assinada por Antonio Terceiro](certificates.terceiro.asc), que assinou
  os certificados em papel.

Para verificar a autenticidade da listas, faça o download dos arquivos e
execute os seguintes comandos:

```
# Daniel Lenharo
$ gpg --recv-keys 31D80509460EFB31DF4B9629FB0E132DDB0AA5B1
$ gpg --verify certificates.lenharo.asc

# Antonio Terceiro
$ gpg --recv-keys B2DEE66036C40829FCD0F10CFC0DB1BBCD460BDE
$ gpg --verify certificates.terceiro.asc
```

## Instruções

Participantes que necessitam de um certificado de participação devem adotar o
seguinte procedimento:

* Fazer o download do [modelo de certificado](../minidc_certf-part.pdf)
* Imprimir, preencher o seu nome e trazer o certificado impresso.
* Solicitar assinatura durante o evento.
* Os certificados devem ser entregues na secretaria do evento até o final de
  sexta-feira (13/04). Os certificados assinados serão entregues no sábado
  (14/04) durante o intervalo da tarde.

Certificados para voluntários

Voluntários na organização que necessitam de um certificado devem adotar o
seguinte procedimento:

* Fazer o download do [modelo de certificado](../minidc_certf-volun.pdf)
* Imprimir, preencher o seu nome e trazer o certificado impresso. Não preencher
  o campo da carga horária.
* Solicitar assinatura durante o evento.
* Os certificados devem ser entregues na secretaria do evento até às 13h do
  sábado (14/04) para ser assinado. Os certificados assinados serão devolvidos
  no sábado até o final do evento.
* Se você não for ao evento no sábado, é só ir até a secretaria pedir para ser
  assinado na sexta-feira.

**Não se preocupe quanto a rigidez dos procedimentos acima. Se tiver qualquer
dúvida, nos procure durante o evento que teremos o maior prazer em resolver a
questão. O importante é você sair do evento com o certificado já assinado
porque não iremos emitir certificados após o evento.**
