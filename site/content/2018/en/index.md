---
title: Home
---

# MiniDebConf Curitiba 2018

Welcome to the MiniDebConf Curitiba 2018 website. The conference will take
place from April 11th to 14th, 2018, at the central Campus of the Federal
Technological University or Paraná (UTFPR). [Learn more](about/).

## Talks videos

Links to the talk recordings are available at the
[schedule page](schedule).

## Photos

[Flickr Album](https://www.flickr.com/photos/curitibalivre/albums/72157690015019780)

<!--

Registration for attendance if free and [can be done here](registration/).

If you want to help us organize this event, take a look at this
[page](volunteers/).
-->

## Gold Sponsors

  [link-collabora]: https://www.collabora.com
  [image-collabora]: /2018/images/logo-collabora.png "Collabora"

 [link-ae]: http://wwww.alhambra-eidos.com.br
 [image-ae]: /2018/images/logo-ae.png "Alhambra-Eidos"

[![Collabora][image-collabora]][link-collabora]
[![Alhambra-Eidos][image-ae]][link-ae]

## Silver Sponsors

  [link-4linux]: http://www.4linux.com.br
  [image-4linux]: /2018/images/logo-4linux.png "4linux"

  [link-polilinux]: http://www.polilinux.com.br
  [image-polilinux]: /2018/images/logo-polilinux.png "PoliLinux"

[![4linux][image-4linux]][link-4linux]
[![PoliLinux][image-polilinux]][link-polilinux]

## Supporters

 [link-utfpr]: http://portal.utfpr.edu.br
  [image-utfpr]: /2018/images/logo-utfpr.png "UTFPR"

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /2018/images/logo-apufpr.png "Apufpr"

  [link-asl]: http://asl.org.br
  [image-asl]: /2018/images/logo-asl.png "ASL"

  [link-pslbrasil]: http://softwarelivre.org
  [image-pslbrasil]: /2018/images/logo-pslbrasil.png "PSL-Brasil"

  [link-coopfam]: http://www.coopfam.agr.br
  [image-coopfam]: /2018/images/logo-coopfam.png "COOPFAM"

  [link-cafefamiliar]: http://www.coopfam.agr.br
  [image-cafefamiliar]: /2018/images/logo-cafe-familiar.png "Café Familiar"

[![UTFPR][image-utfpr]][link-utfpr]
[![Apufpr][image-apufpr]][link-apufpr]
[![ASL][image-asl]][link-asl]
[![PSL-Brasil][image-pslbrasil]][link-pslbrasil]
[![COOPFAM][image-coopfam]][link-coopfam]
[![Café Familiar][image-cafefamiliar]][link-cafefamiliar]

## Organization

  [link-debianbrasil]: http://debianbrasil.org.br
  [image-debianbrasil]: /2018/images/logo-debianbrasil.png "Debian Brasil"

  [link-curitibalivre]: http://curitibalivre.org.br
  [image-curitibalivre]: /2018/images/logo-curitibalivre.png "Curitiba Livre"

[![Debian Brasil][image-debianbrasil]][link-debianbrasil]
[![Curitiba Livre][image-curitibalivre]][link-curitibalivre]
