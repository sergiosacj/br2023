---
title: Sponsors
---

## Gold Sponsors

  [link-collabora]: https://www.collabora.com.br
  [image-collabora]: /2018/images/logo-collabora.png "Collabora"

  [link-ae]: http://www.alhambra-eidos.com.br
  [image-ae]: /2018/images/logo-ae.png "Alhambra-Eidos"

[![Collabora][image-collabora]][link-collabora]
[![Alhambra-Eidos][image-ae]][link-ae]

## Silver Sponsors

  [link-4linux]: http://www.4linux.com.br
  [image-4linux]: /2018/images/logo-4linux.png "4linux"


  [link-polilinux]: http://www.polilinux.com.br
  [image-polilinux]: /2018/images/logo-polilinux.png "PoliLinux"

[![4linux][image-4linux]][link-4linux]
[![PoliLinux][image-polilinux]][link-polilinux]

## Supporters

  [link-utfpr]: http://portal.utfpr.edu.br
  [image-utfpr]: /2018/images/logo-utfpr.png "UTFPR"

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /2018/images/logo-apufpr.png "Apufpr"

  [link-asl]: http://asl.org.br
  [image-asl]: /2018/images/logo-asl.png "ASL"

  [link-pslbrasil]: http://softwarelivre.org
  [image-pslbrasil]: /2018/images/logo-pslbrasil.png "PSL-Brasil"

  [link-coopfam]: http://www.coopfam.agr.br
  [image-coopfam]: /2018/images/logo-coopfam.png "COOPFAM"

  [link-cafefamiliar]: http://www.coopfam.agr.br
  [image-cafefamiliar]: /2018/images/logo-cafe-familiar.png "Café Familiar"

[![UTFPR][image-utfpr]][link-utfpr]
[![Apufpr][image-apufpr]][link-apufpr]
[![ASL][image-asl]][link-asl]
[![PSL-Brasil][image-pslbrasil]][link-pslbrasil]
[![COOPFAM][image-coopfam]][link-coopfam]
[![Café Familiar][image-cafefamiliar]][link-cafefamiliar]

