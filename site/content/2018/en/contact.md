---
title: Contact
---

# Contact

If you need more information about MiniDebConf Curitiba 2018 send a message to the organization:

* E-mail: <brasil.mini@debconf.org>
* E-mail list: <debian-br-eventos@lists.alioth.debian.org>
* IRC Channel: #debian-br-eventos no irc.debian.org

Decentralized social networks:

* [Debian Brasil GNU Social/StatusNet](https://quitter.se/debianbrasil)
* [Debian Brasil on Pump.io](https://identi.ca/debianbrasil)
* [Debian Brasil on Hubzilla](https://hub.vilarejo.pro.br/channel/debianbrasil)

Centralized social networks:

* [Debian Brasil on Telegram](https://t.me/debianbrasil)
* [Debian Brasil on Twitter](https://twitter.com/debianbrasil)
* [Debian Brasil on Facebook](https://www.facebook.com/DebianBrasil/)