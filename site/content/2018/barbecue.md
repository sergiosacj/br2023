---
title: Churrasco
---

# Churrasco

No dia 15 de abril (domingo) faremos um churrasco de confraternização na hora do
almoço no salão de festas da APUFPR - Associação dos Professores da UFPR.

Será cobrado R$ 30,00 por pessoa e o pagamento deverá ser realizado na
secretaria da MiniDebConf até o final do evento.

Esse valor dará direito a:

* Comida
* Bebidas não alcoólicas (refrigerantes e sucos).

Quem quiser pode levar suas cervejas. O local tem refrigerador.

Teremos opções para vegetarianos também. Quando for realizar o pagamento, nos
avise se você não for comer carne.

A APUFPR fica localizada na Rua Dr. Alcides Vieira Arcoverde, 1193 - Jardim das
Américas - Curitiba. O local tem estacionamento gratuito.

A APUFPR fica no caminho para o aeroporto. Então quem for viajar após o
churrasco pode sair de lá de taxi ou de uber direto para o aeroporto para não
precisar voltar para o centro de Curitiba.

