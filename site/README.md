# Dependências

```
$ apt install nanoc rake rerun ruby-gettext
```

# Compilar o site

```
rake server
```

Deixe isso rodando em um terminal, e navegue para
[http://localhost:8888/](http://localhost:8888/). O site é recompilado
automaticamente em função de alterações no conteúdo.

# Estrutura:

* `content/`: conteúdo do site
* `layouts/`: templates HTML
* `output/`: site estático resultante
